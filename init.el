;; Basic environment setup
;;  Until I get a better understanding of Emacs, I am just stealing
;;  from everyone else. Sources mentioned wherever I remember them.
;;
;; Much inspiration taken from http://www.aaronbedra.com/emacs.d/

;; Make the Common Lisp toolbox available.
(require 'cl)

;; Set up package management.
(load "package")
(package-initialize)
(add-to-list 'package-archives
	     '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(setq package-archive-enable-alist '(("melpa" deft magit)))

;; Install my preferred core packages.
;;  Okay yes. Half of these I only know about because of the Aaron Bedra
;;  page listed above. Still - they look really cool.
(defvar bwisti/packages '(auto-complete
			  autopair
			  coffee-mode
			  deft
			  evil
			  gist
			  go-mode
			  groovy-mode
			  haml-mode
			  jade-mode
			  js2-mode
			  magit
			  markdown-mode
			  org
			  vagrant
			  yaml-mode))

(defun bwisti/packages-installed-p ()
  (loop for pkg in bwisti/packages
	when (not (package-installed-p pkg)) do (return nil)
	finally (return t)))

(unless (bwisti/packages-installed-p)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (dolist (pkg bwisti/packages)
    (when (not (package-installed-p pkg))
      (package-install pkg))))

;;
;; appearance
;;
(setq inhibit-splash-screen t
      initial-scratch-message nil)

;; Remove unused GUI elements
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

;; Show end of line.
(setq-default indicate-empty-lines t)
(when (not indicate-empty-lines)
  (toggle-indicate-empty-lines))

;; No tabs!
(setq tab-width 2
      indent-tabs-mode nil)

;; Show matching parentheses
(show-paren-mode t)
(setq column-number-mode t)

(setq echo-keystrokes 0.1
      use-dialog-box nil
      visible-bell t)

;;
;; VIM emulation
;; Remember: C-z toggles emacs-state
;;
(setq evil-search-module 'evil-search
      evil-want-C-u-scroll t
      evil-want-C-w-in-emacs-state t)
(require 'evil)
(evil-mode t)

;; Org Mode niftiness.
;; M-x org-agenda
(setq org-log-done t)
(setq org-todo-keywords
      '((sequence "TODO" "INPROGRESS" "DONE")))
(setq org-todo-keyword-faces
      '(("INPROGRESS" . (:foreground "blue" :weight bold))))
(setq org-agenda-files (list "~/Dropbox/org/personal.org"))

;; deft note-taking
(setq deft-directory "~/Dropbox/deft")
(setq deft-use-filename-as-title t)
(setq deft-extension "org")
(setq deft-text-mode 'org-mode)

;; Automatically add matching braces.
(require 'autopair)

;; Auto completion
(require 'auto-complete-config)
(ac-config-default)

;; Behavior for the emacs Spell checker
(setq flyspell-issue-welcome-flag nil)
(setq-default ispell-program-name "/opt/local/bin/aspell")
(setq-default ispell-list-command "list")

;;
;; Language Hooks
;;

;; Ruby
(add-hook 'ruby-mode-hook
	  (lambda ()
	    (autopair-mode)))
(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.ru$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Rakefile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Capfile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Vagrantfile" . ruby-mode))

;; JavaScript
(add-hook 'js2-mode-hook
	  (lambda ()
	    (autopair-mode)))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;; CoffeeScript
(defun coffee-custom ()
  "coffee-mode-hook"
  (make-local-variable 'tab-width)
  (set 'tab-width 2))
(add-hook 'coffee-mode-hook 'coffee-custom)
(add-to-list 'auto-mode-alist '("\\.coffee\\'" . coffee-mode))

;; Markdown
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.mkd$" . markdown-mode))
(add-hook 'markdown-mode-hook (lambda () (visual-line-mode t)))

;; Improved Perl mode
(defalias 'perl-mode 'cperl-mode)

;;
;; Keybindings
;;
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)

;; via https://sites.google.com/site/steveyegge2/effective-emacs
;; Invoke M-x without Alt
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)
