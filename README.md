# My Emacs Init

I am learning how to use Emacs. This repo proves it.

It contains my customizations as I learn my way around. Well, the ones that are safe to share.

It assumes I will be using these settings on GNU Emacs 24.

[Aaron Bedra's Emacs 24 Configuration]: http://www.aaronbedra.com/emacs.d/

A lot - and I mean a *lot* was taken from [Aaron Bedra's Emacs 24 Configuration][].
To be honest, I will probably take a bit more from there once I understand what
I already have.
